<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,
      shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tienda</title>
    <!-- Font Awesome -->
    <link rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav class="mb-1 navbar navbar-expand-lg navbar-dark primary-color
        lighten-1">
        <a class="navbar-brand" href="#">
          <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png"
            height="30" alt="mdb logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent-555"
          aria-controls="navbarSupportedContent-555" aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Productos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contactos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Informacion</a>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto nav-flex-icons">
            <li class="nav-item avatar">
              <?php
              session_start();
              if(isset($_SESSION['user_image'])){
              ?>
              <a class="nav-link p-0" href="#">
                <img
                  src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg"
                  class="rounded-circle z-depth-0"
                  alt="avatar image" height="35">
              </a>
              <?php }else{ ?>
                <a class="nav-link p-0" role="button" href="pagelogin.php">
                  login
              </a>
              <?php } ?>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <div>
      <!--Carousel Wrapper-->
      <div id="carousel-example-2" class="carousel slide carousel-fade"
        data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-2" data-slide-to="0"
            class="active"></li>
          <li data-target="#carousel-example-2" data-slide-to="1"></li>
          <li data-target="#carousel-example-2" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <div class="view">
              <img class="d-block w-100"
                src="https://www.tumercadazo.com/uploads/banners/proteinas%20con%20envio_88.png"
                alt="First slide">
              <div class="mask rgba-black-light"></div>
            </div>
            <div class="carousel-caption">
              <h3 class="h3-responsive">Light mask</h3>
              <p>First text</p>
            </div>
          </div>
          <div class="carousel-item">
            <!--Mask color-->
            <div class="view">
              <img class="d-block w-100"
                src="https://www.tumercadazo.com/uploads/banners/2x1audifonos_95.png"
                alt="Second slide">
              <div class="mask rgba-black-strong"></div>
            </div>
            <div class="carousel-caption">
              <h3 class="h3-responsive">Strong mask</h3>
              <p>Secondary text</p>
            </div>
          </div>
          <div class="carousel-item">
            <!--Mask color-->
            <div class="view">
              <img class="d-block w-100"
                src="https://www.tumercadazo.com/uploads/banners/buzz%20layer-2_16.png"
                alt="Third slide">
              <div class="mask rgba-black-slight"></div>
            </div>
            <div class="carousel-caption">
              <h3 class="h3-responsive">Slight mask</h3>
              <p>Third text</p>
            </div>
          </div>
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2"
          role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-2"
          role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
      </div>
      <!--/.Carousel Wrapper-->
    </div>


    <div class="container" style="margin-top:10px">
      <div class="row">
        <div class="col-lg-3">
          <div class="card">
            <div class="view">
              <img
                src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
                class="card-img-top"
                alt="photo">
              <a href="#">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <p class="card-text"> A su domicilio o recoja en tienda. GRATIS en
                ciudades principales.(La Paz, Cochabamba, Santa Cruz)</p>
            </div>
          </div>

        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="view">
                  <img
                    src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
                    class="card-img-top"
                    alt="photo">
                  <a href="#">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text"> Tarjeta de Crédito/Débito, Transferencia/Depósito bancario, PayPal,
                      TigoMoney y más.</p>
                </div>
              </div>
    
         
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="view">
                  <img
                    src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
                    class="card-img-top"
                    alt="photo">
                  <a href="#">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text"> 
                      Todos los productos ofertados son nuevos y cuentan con garantía real.</p>
                </div>
              </div>
    
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="view">
                  <img
                    src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
                    class="card-img-top"
                    alt="photo">
                  <a href="#">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text"> 
                      No dude en ponerse en contacto con nosotros. Su pregunta no molesta.</p>
                </div>
              </div>
        </div>
      </div>
    </div>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
  </body>

</html>
