create database tienda;
use tienda;
create table usuario(
    id int primary key auto_increment,
    nombre varchar(500),
    email varchar(500),
    password varchar(500),
    phone varchar(20),
    photo text
);
create table mensaje(
    id int primary key auto_increment,
    email varchar(500),
    asunto varchar(200),
    detalle text
);
create table categoria(
    id int primary key auto_increment,
    nombre varchar(100),
    descripcion text
);

create table producto(
    id int primary key auto_increment,
    nombre varchar(200),
    descripcion text,
    precio varchar(20),
    foto text,
    idcat int,
    foreign key (idcat) references categoria(id)
);