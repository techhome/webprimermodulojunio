<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

    <h3>Sumar</h3>
    <form action="sumar.php" method="post">
        <input type="text" name="num1">
        <input type="text" name="num2">
        <input type="submit" value="enviar">
    </form>
    <h3>Saludar</h3>
    <form action="saludar.php" method="get">
        <input type="text" name="nombre">
        <input type="submit" value="enviar">
    </form>

    <?php
$vector = ["hola", "como", "estas", "pedro",
    "juan", "marco", "docker", "react", "vue", "data",
    "python", "php"];
$vectorkeys = ["juan" => "21", "pedro" => "29", "victor" => "24"];
//{juan:21, pedro:29 ,victor:24}
for ($i = 0; $i < count($vector); $i++) {
    ?>
    <div>
            <h5><?php echo $vector[$i]; ?></h5>
        </div>
    <?php
}
echo "<br>";
foreach ($vectorkeys as $key => $value) {
    echo "$key : $vectorkeys[$key] <br>";
}
$juan = ["edad" => "21", "foto" => "https://lakewangaryschool.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg",
    "description" => "es el mejor"];
$pedro = ["edad" => "24", "foto" => "https://workhound.com/wp-content/uploads/2017/05/placeholder-profile-pic.png", "description" => "delgado alto cabello negro"];
$luis = ["edad" => "28", "foto" => "https://image.shutterstock.com/image-vector/male-profile-picture-placeholder-vector-260nw-228952291.jpg", "description" => "ojos claros cabellero negro alto"];
$juliana = ["edad" => "20", "foto" => "http://www.broadwaverly.com/wp-content/uploads/2018/02/profile-placeholder-female.png", "description" => "cabello largo, ojos verdes"];
$vector2 = ["juan" => $juan, "pedro" => $pedro, "luis" => $luis, "juliana" => $juliana];
//echo "aqui hay codigo php";
?>
<div class="container">
    <div class="row">
        <?php
foreach ($vector2 as $key => $value) {
    ?>
        <div class="col-lg-3">
        <div class="card" >
        <img src="<?php echo $value["foto"]; ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?php echo $key; ?></h5>
                <h6>Edad: <?php echo $value["edad"]; ?></h6>
                <p class="card-text"><?php echo $value["description"]; ?></p>
                <a href="#" class="btn btn-primary">Ver Detalle</a>
            </div>
        </div>
        </div> 
    </div>
<?php } ?>
</div>
</body>
</html>