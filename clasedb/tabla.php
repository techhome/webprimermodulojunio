<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="text-center">
        <h1>Lista de Productos</h1>
</div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Descripcion</th>
      <th scope="col">Foto</th>
      <th scope="col">Precio</th>
    </tr>
  </thead>
  <tbody>
  <?php
    require_once 'config.php';
    $db = ConectarDB();
    $productos = $db->query("select * from producto");
    $count = 0;
    foreach($productos as $product){
        $count++;
    ?>
    <tr>
        <td><?php echo $count; ?></td>
      <td><?php echo $product['id']; ?></td>
      <td><?php echo $product['nombre']; ?></td>
      <td><?php echo $product['descripcion']; ?></td>
      <td><?php echo $product['foto']; ?></td>
      <td><?php echo $product['precio']; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
   
</body>
</html>